import numpy as np
from imutils import paths
import argparse
import cv2
import requests
import json

headers = {'Content-type': 'application/json','Accept-Charset': 'UTF-8'}
data = '{"login":"client1","password":"client1"}'
response = requests.post('http://localhost:3001/client-detection/login', headers=headers, data=data)
json_response = response.json()
api_key = json_response["token"]
#token comme variable denvironnemment
#api_key = os.environ.get('API_KEY')
if api_key is None:
    print('server is down')
    exit(1)

def variance_of_laplacian(image):
	# compute the Laplacian of the image and then return the focus
	# measure, which is simply the variance of the Laplacian
	return cv2.Laplacian(image, cv2.CV_64F).var()

#l'url du camera a surveiller
#cap = cv2.VideoCapture("rtsp://admin:samsung123@192.168.2.102/onvif/profile1/media.smp")
cap = cv2.VideoCapture(0)
cap.set(3, 200)
cap.set(4, 200)
while(1):

	# Take each frame
	_, frame = cap.read()
	
	# Convert BGR to HSV
	hsv = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
	fm = variance_of_laplacian(hsv)
	

	text =""
	if fm < 100.0:
		text="Blurry"
	else:
		text="Not Blurry"

	if frame is None :
		data1 = '{"details":"pas dimage","type":"panne","image": "none"}'
		response1 = requests.post('http://localhost:3001/client-detection/event', auth=requests.auth.HTTPBasicAuth(api_key, ''), headers=headers, data=data1)
		print "camera en panne"
		print response1


	if text =="Blurry":
		data2 = '{"details":"image floue","type":"information","image": "none"}'
		response2 = requests.post('http://localhost:3001/client-detection/event', auth=requests.auth.HTTPBasicAuth(api_key, ''), headers=headers, data=data2)
		print "qualite floue"
		print api_key
		print response2



	cv2.putText(frame, "{}: {:.2f}".format(text,fm),(10,30),cv2.FONT_HERSHEY_SIMPLEX,0.8,(255,255,0),3)
	cv2.imshow('frame', frame)	

	k = cv2.waitKey(5) & 0xFF
	if k == 27:
		break

cv2.destroyAllWindows()
