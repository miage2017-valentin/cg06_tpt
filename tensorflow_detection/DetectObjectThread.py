import sys
from threading import Thread
import cv2
from object_detection.utils import label_map_util
from object_detection.utils import visualization_utils as vis_util
import numpy as np
import os
import six.moves.urllib as urllib
import tarfile
import tensorflow as tf
import time
from scipy.stats import itemfreq
import webcolors

class DetectObjectThread(Thread):
    def __init__(self, clientApi, cap):
        Thread.__init__(self)
        self.clientApi = clientApi
        self.stackFrames = []
        self.Terminated = False
        self.cap = cap

    def run(self):
        MODEL_NAME = 'detect_object/ssd_mobilenet_v2_coco_2018_03_29'
        MODEL_FILE = MODEL_NAME + '.tar.gz'
        DOWNLOAD_BASE = 'http://download.tensorflow.org/models/object_detection/'
        PATH_TO_CKPT = MODEL_NAME + '/frozen_inference_graph.pb'
        PATH_TO_LABELS = os.path.join('detect_object/research/object_detection/data', 'mscoco_label_map.pbtxt')

        line_thickness = 6
        width = self.cap.get(3) 
        height = self.cap.get(4)

        NUM_CLASSES = 90
        detection_graph = tf.Graph()
        with detection_graph.as_default():
            od_graph_def = tf.GraphDef()
            with tf.gfile.GFile(PATH_TO_CKPT, 'rb') as fid:
                serialized_graph = fid.read()
                od_graph_def.ParseFromString(serialized_graph)
                tf.import_graph_def(od_graph_def, name='')
        
        label_map = label_map_util.load_labelmap(PATH_TO_LABELS)
        categories = label_map_util.convert_label_map_to_categories(label_map, max_num_classes=NUM_CLASSES, use_display_name=True)
        category_index = label_map_util.create_category_index(categories)
        with detection_graph.as_default():
            with tf.Session(graph=detection_graph) as sess:
                while not self.Terminated:
                    if(len(self.stackFrames) > 0):
                        frame = self.stackFrames.pop(0)
                        if frame['ret'] == True:
                            image_np_expanded = np.expand_dims(frame['image_np'], axis=0)
                            image_tensor = detection_graph.get_tensor_by_name('image_tensor:0')
                            boxes = detection_graph.get_tensor_by_name('detection_boxes:0')
                            scores = detection_graph.get_tensor_by_name('detection_scores:0')
                            classes = detection_graph.get_tensor_by_name('detection_classes:0')
                            num_detections = detection_graph.get_tensor_by_name('num_detections:0')
                            (boxes, scores, classes, num_detections) = sess.run(
                                [boxes, scores, classes, num_detections],
                                feed_dict={image_tensor: image_np_expanded})
                            vis_util.visualize_boxes_and_labels_on_image_array(
                                frame['image_np'],
                                np.squeeze(boxes),
                                np.squeeze(classes).astype(np.int32),
                                np.squeeze(scores),
                                category_index,
                                use_normalized_coordinates=True,
                                line_thickness=line_thickness)
                            
                            objects = []
                            for index, value in enumerate(classes[0]):
                                object_dict = {}
                                if scores[0, index] > 0.5:
                                    typeObject = (category_index.get(value)).get('name')
                                    object_dict[(category_index.get(value)).get('name').encode('utf8')] = \
                                                scores[0, index]
                                    
                                    # Get coord of object
                                    ymin = int(boxes[0][index][0]*height)+line_thickness
                                    xmin = int(boxes[0][index][1]*width)+line_thickness
                                    ymax = int(boxes[0][index][2]*height)-line_thickness
                                    xmax = int(boxes[0][index][3]*width)-line_thickness

                                    if ymin < ymax and xmin < xmax:
                                        crop_img = frame['image_np'][ymin:ymax, xmin:xmax]
                                        color = self.dominant_color(crop_img)
                                    print(typeObject)
                                    print(self.clientApi.login())
                                    resize_detect = cv2.resize(frame['image_np'], (1280,720))
                                    image = cv2.imencode(".jpg", resize_detect)[1]
                                    uploadFileInfo = self.clientApi.uploadFile('detect_people', image.tostring(), "image/jpeg")
                                    print(self.clientApi.addLog(color, typeObject, uploadFileInfo['idFile'], frame['camera']))
                    else:
                        time.sleep(1)



    def load_image_into_numpy_array(self, image):
        (im_width, im_height) = image.size
        return np.array(image.getdata()).reshape(
            (im_height, im_width, 3)).astype(np.uint8)

    def addFrames(self, ret, image_np, camera):
        self.stackFrames.append({'ret': ret, 'image_np': image_np, 'camera': camera})

    def stop(self):
        self.Terminated = True

    def dominant_color(self, np_object):
        pixs = np_object.reshape((-1, 3))

        n_colors = 5
        flags = cv2.KMEANS_RANDOM_CENTERS
        criteria = (cv2.TERM_CRITERIA_EPS + cv2.TERM_CRITERIA_MAX_ITER, 200, .1)
        _, labels, centroids = cv2.kmeans(np.float32(pixs), n_colors, None, criteria, 10, flags)

        palette = np.uint8(centroids)
        dominant_object_color = palette[np.argmax(itemfreq(labels)[:, -1])]
        # try:
        #     actual_name = webcolors.rgb_to_name(dominant_object_color)
        # except ValueError:
        #     actual_name = self.closest_colour(dominant_object_color)
        return dominant_object_color
    
    def closest_colour(self, requested_colour):
        min_colours = {}
        for key, name in webcolors.css3_hex_to_names.items():
            r_c, g_c, b_c = webcolors.hex_to_rgb(key)
            rd = (r_c - requested_colour[0]) ** 2
            gd = (g_c - requested_colour[1]) ** 2
            bd = (b_c - requested_colour[2]) ** 2
            min_colours[(rd + gd + bd)] = name
        return min_colours[min(min_colours.keys())]