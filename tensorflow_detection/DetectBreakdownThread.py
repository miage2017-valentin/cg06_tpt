import numpy as np
from imutils import paths
import sys
from threading import Thread
import cv2
import time

class DetectBreakdownThread(Thread):
    def __init__(self, clientApi, cap):
        Thread.__init__(self)
        self.clientApi = clientApi
        self.stackFrames = []
        self.Terminated = False
        self.cap = cap

    def run(self):
        while not self.Terminated:
            if(len(self.stackFrames) > 0):
                frame = self.stackFrames.pop(0)
                if frame['ret'] == True:
                    im = frame['image_np']
                    hsv = cv2.cvtColor(im, cv2.COLOR_BGR2GRAY)
                    fm = self.variance_of_laplacian(hsv)

                    print(fm)
                    text =""
                    if fm < 70.0:
                        text="Blurry"
                    else:
                        text="Not Blurry"

                    if frame is None :
                        print(self.clientApi.createEvent("Breakdown", "No image", None, frame['camera']))

                    if text =="Blurry":
                        resize_detect = cv2.resize(im, (1280,720))
                        image = cv2.imencode(".jpg", resize_detect)[1]
                        uploadFileInfo = self.clientApi.uploadFile('breakdown', image.tostring(), "image/jpeg")
                        print(self.clientApi.createEvent("Breakdown", "Visual blurring", uploadFileInfo['idFile'], frame['camera']))
            else:
                time.sleep(1)

    def addFrames(self, ret, image_np, camera):
        self.stackFrames.append({'ret': ret, 'image_np': image_np, 'camera': camera})

    def stop(self):
        self.Terminated = True

    def variance_of_laplacian(self,image):
        # compute the Laplacian of the image and then return the focus
        # measure, which is simply the variance of the Laplacian
        return cv2.Laplacian(image, cv2.CV_64F).var()