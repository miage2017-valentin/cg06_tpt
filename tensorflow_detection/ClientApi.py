from dotenv import load_dotenv
import requests
import os
load_dotenv()


class ClientApi:
    def __init__(self):
        self.urlAPI = os.getenv("URL_API")
        self.username = os.getenv("LOGIN")
        self.password = os.getenv("PASSWORD")

    def login(self):
        bodyJson = { 'login' : self.username, 'password': self.password}
        r = requests.post(self.urlAPI + "/client-detection/login", json = bodyJson)
        result = r.json()
        self.token = result['token']
        self.headerWithAuthorization =  headers={'Authorization': 'Bearer ' + self.token}
        return result

    def uploadFile(self, filename, fileContent, mimeType):
        files = { 'file': (filename,fileContent, mimeType) }
        r = requests.post(self.urlAPI + "/client-detection/upload", headers = self.headerWithAuthorization, files=files)
        return r.json() 
    
    def createEvent(self, type, details, filenameImage, camera):
        bodyJson = { 'type': type, 'details': details, 'image': filenameImage, 'camera': camera}
        r = requests.post(self.urlAPI + "/client-detection/event", headers = self.headerWithAuthorization, json=bodyJson)
        return r.json() 

    def checkIfVehicleIsAuthorized(self, vehicleNumber):
        r = requests.get(self.urlAPI + "/client-detection/checkVehicleAuthorized/"+vehicleNumber, headers = self.headerWithAuthorization)
        return r.json()

    def addLog(self, color, typeObject, filenameImage, camera):
        rgbColor = { 'r': str(color[0]), 'g': str(color[1]), 'b': str(color[2])}
        bodyJson = { 'color': rgbColor, 'type': typeObject, 'image': filenameImage, 'camera': camera}
        r = requests.post(self.urlAPI + "/client-detection/log", headers = self.headerWithAuthorization, json=bodyJson)
        return r.json() 
    
    def getCameraList(self):
        r = requests.get(self.urlAPI + "/client-detection/camera", headers = self.headerWithAuthorization)
        if r.text == '':
            return []
        return r.json()
