import sys
from threading import Thread
import time
import cv2
from DetectObjectThread import DetectObjectThread
from DetectPlateThread import DetectPlateThread
from DetectBreakdownThread import DetectBreakdownThread

class CameraThread(Thread):
    def __init__(self, camera, clientApi):
        Thread.__init__(self)
        self.camera = camera
        self.clientApi = clientApi

    def run(self):
        print(self.camera['reference'])
        cap = cv2.VideoCapture(self.camera['url'])
        cv2.namedWindow(self.camera['reference'])
        fps = 30
        size = (int(cap.get(cv2.CAP_PROP_FRAME_WIDTH)), int(cap.get(cv2.CAP_PROP_FRAME_HEIGHT)))
        fourcc = cv2.VideoWriter_fourcc(*'XVID')
        thread = DetectObjectThread(self.clientApi, cap)
        thread.start()
        threadDetectPlate = DetectPlateThread(self.clientApi, cap)
        threadDetectPlate.start()
        threadBreakdown = DetectBreakdownThread(self.clientApi, cap)
        threadBreakdown.start()

        #out = cv2.VideoWriter('output.avi',fourcc, 30.0, (640,480))
        while cap.isOpened():
            ret, image_np = cap.read()

            threadBreakdown.addFrames(ret, image_np, self.camera['_id'])
            if(image_np is None):
                break
            thread.addFrames(ret, image_np, self.camera['_id'])
            #threadDetectPlate.addFrames(ret, image_np, self.camera['_id'])
            frame = cv2.flip(image_np,1)
            #out.write(image_np)
            resize = cv2.resize(image_np, (640,480))
            cv2.imshow(self.camera['reference'],resize)
            #print([category_index.get(i) for i in classes[0]])
            if cv2.waitKey(1) & 0xFF == ord('q'):
                cv2.destroyWindow(self.camera['reference'])
                thread.stop()
                threadDetectPlate.stop()
                threadBreakdown.stop()
                break
        cap.release()
        cv2.destroyWindow(self.camera['reference'])