from ClientApi import ClientApi
from CameraThread import CameraThread

if __name__ == "__main__":
    client = ClientApi()
    print(client.login())
    cameras = client.getCameraList()
    for camera in cameras :
        thread = CameraThread(camera, client)
        thread.start()
