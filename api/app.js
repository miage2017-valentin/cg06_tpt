require('dotenv').config()
const express = require('express')
const app = express()
const mongoose = require('mongoose');
const defaultData = require('./app/models/default-data');
const publicDir = require('path').join(__dirname, '/public');
const bodyParser = require('body-parser');
const boom = require('express-boom');

//Configuration of database
const configBdd = {
    useNewUrlParser: true,
};

mongoose.connect('mongodb://' + process.env.USER_DB + ':' + process.env.PASSWORD_DB +
    '@' + process.env.HOST_DB + ':' + process.env.PORT_DB + '/' + process.env.NAME_DB
    + '?authSource=admin', configBdd)
    .then((err, db) => {
        console.log("Connected with database");
        defaultData.clear().then(() => { defaultData.save(); });
    })
    .catch((error) => {
        console.log("ERROR with database connection : " + error)
        process.exit(1);
    });



app.use(boom());
app.use(bodyParser.json());       // to support JSON-encoded bodies
app.use(bodyParser.urlencoded({ extended: true }));
app.use(function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.setHeader("Access-Control-Allow-Methods", "GET,HEAD,OPTIONS,POST,PUT,DELETE");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization, Access-Control-Request-Method, Access-Control-Request-Headers");
    next();
});
app.use("/public", express.static(publicDir));


// Routes
const adminRoutes = require("./app/routes_admin.js")();
const clientTfDetectionRoutes = require("./app/routes_clientTfDetection.js")();
app.use('/', adminRoutes);
app.use('/client-detection/', clientTfDetectionRoutes);

app.listen(process.env.PORT, () => {
    console.log('API on port ' + process.env.PORT);
})






//    socket.emit('notify', { hello: 'world' });