const express = require('express');
const UserController = require("./controllers/UserController");
const VehicleController = require("./controllers/VehicleController");
const LogController = require("./controllers/LogController");
const FileController = require("./controllers/FileController");
const EventController = require("./controllers/EventController");
const CameraController = require('./controllers/CameraController');
const ClientTfDetectionController = require('./controllers/ClientTfDetectionController');
const router = express.Router();
const AuthMiddleware = require('./middlewares/authMiddleware');
const multer  = require('multer');
const upload = multer({ dest: 'public/images/' });
const models = require("./models");

module.exports = () => {
    const vehicleController = new VehicleController();
    const userController = new UserController();
    const authMiddleware = new AuthMiddleware(models.User);
    const eventController = new EventController();
    const logController = new LogController();
    const fileController = new FileController();
    const cameraController = new CameraController();
    const clientTfDetectionController = new ClientTfDetectionController();

    router.post('/login', authMiddleware.login.bind(authMiddleware));
    router.post('/register', userController.register);
    router.get('/checkToken', authMiddleware.checkToken.bind(authMiddleware));
    router.get('/profile', authMiddleware.getMiddleware.bind(authMiddleware), authMiddleware.profile.bind(authMiddleware));

    // Image
    router.get('/file/:id', authMiddleware.getMiddleware.bind(authMiddleware), fileController.file);

    // Vehicle
    router.post('/vehicle', authMiddleware.getMiddleware.bind(authMiddleware), vehicleController.createVehicle);
    router.get('/vehicles', authMiddleware.getMiddleware.bind(authMiddleware), vehicleController.getVehiclesAuthorized)
    router.delete('/vehicle/:id', authMiddleware.getMiddleware.bind(authMiddleware), vehicleController.deleteVehicleAuthorized)
    
    // Event
    router.get('/events', authMiddleware.getMiddleware.bind(authMiddleware), eventController.getEvents);
    router.get('/logs', authMiddleware.getMiddleware.bind(authMiddleware), logController.getLogs);

    // Camera
    router.post('/camera', authMiddleware.getMiddleware.bind(authMiddleware), cameraController.createCamera);
    router.put('/camera/:id', authMiddleware.getMiddleware.bind(authMiddleware), cameraController.updateCamera);
    router.get('/cameras', authMiddleware.getMiddleware.bind(authMiddleware), cameraController.getAllCamera)
    router.delete('/camera/:id', authMiddleware.getMiddleware.bind(authMiddleware), cameraController.deleteCamera);

    // Client Tensorflow Detection
    router.post('/client', authMiddleware.getMiddleware.bind(authMiddleware), clientTfDetectionController.createClientDetection);
    router.put('/client/:id', authMiddleware.getMiddleware.bind(authMiddleware), clientTfDetectionController.updateClientDetection);
    router.get('/clients', authMiddleware.getMiddleware.bind(authMiddleware), clientTfDetectionController.getAllClientDetection);
    router.delete('/client/:id', authMiddleware.getMiddleware.bind(authMiddleware), clientTfDetectionController.deleteClient);

    return router;
};