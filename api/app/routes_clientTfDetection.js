const express = require('express');
const EventController = require("./controllers/EventController");
const LogController = require("./controllers/LogController");
const VehicleController = require("./controllers/VehicleController");
const FileController = require("./controllers/FileController");
const CameraController = require('./controllers/CameraController');
const router = express.Router();
const AuthMiddleware = require('./middlewares/authMiddleware');
const models = require("./models");

module.exports = (io) => {
    const vehicleController = new VehicleController();
    const eventController = new EventController();
    const fileController = new FileController();
    const authMiddleware = new AuthMiddleware(models.ClientTfDetection);
    const logController = new LogController();
    const cameraController = new CameraController();

    // User
    router.post('/login', authMiddleware.login.bind(authMiddleware));
    router.get('/checkToken', authMiddleware.checkToken.bind(authMiddleware));
    router.get('/profile', authMiddleware.getMiddleware.bind(authMiddleware), authMiddleware.profile.bind(authMiddleware));

    // Upload file
    router.post('/upload', authMiddleware.getMiddleware.bind(authMiddleware), fileController.upload);

    // Event
    router.post('/event', authMiddleware.getMiddleware.bind(authMiddleware), eventController.createEvent.bind(eventController));
    router.get('/checkVehicleAuthorized/:vehicleNumber', authMiddleware.getMiddleware.bind(authMiddleware), vehicleController.checkVehicleIsAuthorized)

    // Log
    router.post('/log', authMiddleware.getMiddleware.bind(authMiddleware), logController.createLog);

    // Camera
    router.get('/camera', authMiddleware.getMiddleware.bind(authMiddleware), cameraController.getCamera)

    return router;
};