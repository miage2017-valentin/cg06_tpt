const models = require('./index.js');
const bcrypt = require('bcrypt');

module.exports = {
    clear: () => {
        let listPromise = [];
        Object.keys(models).forEach((key, index) => {
            listPromise.push(models[key].deleteMany({}));
        });

        return Promise.all(listPromise);
    },
    save: async () => {
        console.log("Create default data in database...");
        console.log("Create users...");
        // Create Users
        let user1 = new models.User({
            _id: "ee4658b0-1bd8-4757-b2ff-76fc68762acd",
            login: "admin@admin.com",
            password: "admin",
            name: "François",
            lastname: "LeFrançais",
            address: "Quelque part",
            city: "dans cette ville"
        });
        user1.password = await bcrypt.hash(user1.password, parseInt(process.env.SALTROUNDS));
        await user1.save();

        // Client detection
        let clientTfDetection = new models.ClientTfDetection({
            _id: "48b0d072-8500-4eee-a4c1-dd7226be0bce",
            login: "client1",
            password: "client1",
            reference: "client_tf_detection_1"
        });
        clientTfDetection.password = await bcrypt.hash(clientTfDetection.password, parseInt(process.env.SALTROUNDS));
        await clientTfDetection.save();

        let secondClientTfDetection = new models.ClientTfDetection({
            _id: "901b0346-f0d5-41a5-bfc5-16c4470c7bd0",
            login: "client2",
            password: "client2",
            reference: "client_tf_detection_2"
        });
        secondClientTfDetection.password = await bcrypt.hash(secondClientTfDetection.password, parseInt(process.env.SALTROUNDS));
        await secondClientTfDetection.save();

        console.log('Create cameras...');
        // Add camera
        let firstCamera = new models.Camera({
            reference: "CAM-01",
            url: "http://81.21.115.29:80/mjpg/video.mjpg",
            clientAssigned : clientTfDetection
        });
        await firstCamera.save();

        let secondCamera = new models.Camera({
            reference: "CAM-02",
            //url: "http://96.67.45.106:80/mjpg/video.mjpg",
            url: 'video/testDetectPlates.mp4',
            clientAssigned : clientTfDetection
        });
        await secondCamera.save();

        let thirdCamera = new models.Camera({
            reference: "CAM-03",
            url: "http://213.21.162.171:80/mjpg/video.mjpg",
            clientAssigned: clientTfDetection
        });
        await thirdCamera.save();

        clientTfDetection.camera = [firstCamera, secondCamera, thirdCamera];
        clientTfDetection.save();

        console.log("Finish to create default data !")
    }
}