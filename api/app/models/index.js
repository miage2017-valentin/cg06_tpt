const mongoose = require('mongoose');

const UserSchema = new mongoose.Schema({
    _id: String,
    login: {
        type: String,
        required: true,
        unique: true
    },
    password: String,
    name: String,
    lastname: String,
    address: String,
    city: String
});

const ClientTfDetectionSchema = new mongoose.Schema({
    _id: String,
    login: {
        type: String,
        required: true,
        unique: true
    },
    password: String,
    reference: String,
    camera : [{ type: mongoose.Schema.Types.ObjectId, ref: 'Camera' }]
});

const EventSchema = new mongoose.Schema({
    details: {
        type: String,
        required: true
    },
    type: {
        type: String,
        enum: ['Breakdown', 'Unauthorized'],
        required: true
    },
    date: {
        type: Date,
        default: Date.now
    },
    camera: { type: mongoose.Schema.Types.ObjectId, ref: 'Camera', required:true },
    image: String
})

const VehicleSchema = new mongoose.Schema({
    vehicleNumber: { type: String, unique: true, required: true },
    createdDate: { type: Date, default: Date.now }
});

const LogSchema = new mongoose.Schema({
    color: { 
        r: Number,
        g: Number,
        b: Number
    },
    type: {
        type: String,
        required: true
    },
    image: {
        type: String,
        required: true
    },
    camera: { type: mongoose.Schema.Types.ObjectId, ref: 'Camera', required:true },
    createdDate: { type: Date, default: Date.now }
});

const CameraSchema = new mongoose.Schema({
    reference: {
        type: String,
        required: true
    },
    url: {
        type: String,
        unique: true,
        required: true
    },
    clientAssigned : { type: mongoose.Schema.Types.ObjectId, ref: 'ClientTfDetection', required:true }
});

module.exports = {
    User : mongoose.model('User', UserSchema),
    ClientTfDetection: mongoose.model('ClientTfDetection', ClientTfDetectionSchema),
    Event: mongoose.model('Event', EventSchema),
    Vehicle: mongoose.model('Vehicle', VehicleSchema),
    Log: mongoose.model('Log', LogSchema),
    Camera: mongoose.model('Camera', CameraSchema)
};