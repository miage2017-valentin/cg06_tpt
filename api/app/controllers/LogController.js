const models = require("../models");
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const mongoose = require('mongoose');


module.exports = class {

    async createLog(req, res){
        const logInput = req.body;

        const camera = await models.Camera.findById(logInput.camera).exec();
        if(camera === null){
            res.boom.notFound('Camera not found');
            return;
        }

        logInput.camera = camera;

        const log = new models.Log(logInput);
        await log.save();
        res.json(log);
    }

    async getLogs(req, res){
        models.Log.find()
        .populate('camera', '')
        .exec((error, logs) =>{
            res.json(logs);
        });
    }
}