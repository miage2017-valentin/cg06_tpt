const models = require("../models");
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const mongoose = require('mongoose');


module.exports = class {

    async createVehicle(req, res){
        const vehicleInput = req.body;
        console.log(vehicleInput);
        const vehicle = new models.Vehicle(vehicleInput);
        console.log(vehicle)
        await vehicle.save((error) =>{
            console.log(error);
            if(error != null){
                res.boom.badRequest("Fail to add a new number car plate authorized");
                return;
            }
            res.json(vehicle);
        })
    }

    async getVehiclesAuthorized(req, res){
        models.Vehicle.find().exec((error, vehicles) =>{
            res.json(vehicles);
        });
    }

    async checkVehicleIsAuthorized(req, res){
        const vehicleNumber = req.params.vehicleNumber;
        models.Vehicle.find({vehicleNumber: vehicleNumber}).exec((error, vehicles)=>{
            res.status(200).send(vehicles.length > 0);
        })
    }

    async deleteVehicleAuthorized(req, res){
        const id = req.params.id;
        models.Vehicle.deleteOne({'_id' : id}, (err)=>{
            if(err){
                res.boom.badRequest(error);
                return;
            }
            res.status(200).send({message: "Deleted with success"})
        })
    }
}