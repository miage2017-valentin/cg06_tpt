const models = require("../models");
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const mongoose = require('mongoose');

// Init SocketIO
const appSocketIO = require('http').createServer(() => {
    res.writeHead(200, { 'Content-Type': 'text/plain' });
    res.write('SocketIO');
    res.end();
});
const io = require('socket.io')(appSocketIO);
appSocketIO.listen(80);

module.exports = class {

    constructor() {
        io.on('connection', (socket) => {
            console.log(Object.values(io.sockets.sockets));
        });
    }

    async createEvent(req, res) {
        const eventInput = req.body;

        const camera = await models.Camera.findById(eventInput.camera).exec();
        if (camera === null) {
            res.boom.notFound('Camera not found');
            return;
        }

        eventInput.camera = camera;
        const event = new models.Event(eventInput);
        await event.save();
        const eventReturn = await models.Event.findById(event._id)
            .populate('camera', '')
            .exec();
        Object.values(io.sockets.sockets).map(x => x.emit('notify', {}));
        res.json(eventReturn);
    }

    async getEvents(req, res) {
        models.Event.find()
            .populate('camera')
            .exec((error, events) => {
                res.json(events);
            });
    }
}