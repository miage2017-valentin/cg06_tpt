const models = require("../models");
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const mongoose = require('mongoose');


module.exports = class {

    async createCamera(req, res){
        const cameraInput = req.body;
        const clientAssigned = await models.ClientTfDetection.findById(cameraInput.clientAssigned).exec();
        if(clientAssigned === null){
            res.boom.notFound('Client not found');
            return;
        }
        cameraInput.clientAssigned = clientAssigned;
        const camera = new models.Camera(cameraInput);
        try{
            await camera.save();
            clientAssigned.camera.push(camera);
            await clientAssigned.save();
            const cameraReturn = await models.Camera.findById(camera._id)
            .populate('clientAssigned', '-login -password')
            .exec();
            res.json(cameraReturn);
        }catch(err){
            res.boom.badRequest("Fail to add a new camera");
            return;
        }
    }

    async getCamera(req, res){
        console.log(req.currentUser.camera)
        models.Camera.find({_id : {$in : req.currentUser.camera}})
        .populate('clientAssigned', '-login -password')
        .exec((error, cameras) =>{
            console.log(cameras)
            res.json(cameras);
        });
    }

    async getAllCamera(req, res){
        models.Camera.find()
        .populate('clientAssigned', '-login -password')
        .exec((error, cameras) =>{
            res.json(cameras);
        }); 
    }

    async updateCamera(req, res){
        const cameraInput = req.body;
        const id = req.params.id;

        const clientAssigned = await models.ClientTfDetection.findById(cameraInput.clientAssigned).exec();
        if(clientAssigned === null){
            res.boom.notFound('Client not found');
            return;
        }
        const cameraFound = await models.Camera.findById(id).exec();
        if(cameraFound === null){
            res.boom.notFound('Camera not found');
            return;
        }

        let camera = Object.assign(cameraFound, cameraInput);
        camera.clientAssigned = clientAssigned;
        try{
            await camera.save();
            clientAssigned.camera.push(camera);
            await clientAssigned.save();
            const cameraReturn = await models.Camera.findById(camera._id)
            .populate('clientAssigned', '-login -password')
            .exec();
            res.json(cameraReturn);
        }catch(err){
            res.boom.badRequest("Fail to update this camera");
            return;
        }
    }

    async deleteCamera(req, res){
        const id = req.params.id;

        const cameraFound = await models.Camera.findById(id).exec();
        if(cameraFound === null){
            res.boom.notFound('Camera not found');
            return;
        }

        try{
            await models.Camera.deleteOne({_id: id}).exec();
            res.json({message: "Camera deleted with success"});
        }catch(error){
            res.boom.badRequest(error);
        }
    }
}