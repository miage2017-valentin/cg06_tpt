const mongoose = require('mongoose');
const GridFsStorage = require('multer-gridfs-storage');
const Grid = require('gridfs-stream');
const multer = require('multer');
const conn = mongoose.connection;
let gfs = null, storage = null, upload = null, gridFSBucket = null;

conn.once('open', () => {
    gfs = Grid(conn.db, mongoose.mongo);
    storage = GridFsStorage({
        db: conn.db,
        filename: function (req, file, cb) {
            var datetimestamp = Date.now();
            cb(null, file.fieldname + '-' + datetimestamp + '.' + file.originalname.split('.')[file.originalname.split('.').length - 1]);
        },
        metadata: function (req, file, cb) {
            cb(null, { originalname: file.originalname });
        },
        root: 'imageSave' //root name for collection to store files into
    });

    upload = multer({ //multer settings for single upload
        storage: storage
    }).single('file');

    gridFSBucket = new mongoose.mongo.GridFSBucket(conn.db, { bucketName: 'fs' });
})


module.exports = class {

    upload(req, res) {
        upload(req,res,function(err){
            if(err || req.file === undefined){
                res.boom.badData(err);
                return;
            }
            console.log(req.file)
            res.json({error_code:0,err_desc:null, idFile: req.file.id});
        });
    }

    file(req, res){
        gfs.collection('fs');
        /** First check if file exists */
        gfs.files.findOne({"_id": mongoose.Types.ObjectId(req.params.id)}, function(err, file){
            if(file === null){
                res.boom.notFound("File not found");
                return;
            }
            const readstream = gridFSBucket.openDownloadStreamByName(file.filename);
            res.set('Content-Type', file.contentType)
            return readstream.pipe(res);
        });
    }
}