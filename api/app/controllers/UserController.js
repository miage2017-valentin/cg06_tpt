const models = require("../models");
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');

module.exports = class {

    register(req, res) {
        const email = req.body.email;
        const password = req.body.password;
        const passwordConfirm = req.body.passwordConfirm;

        if(password !== passwordConfirm){
            res.status(400).send({ auth: false, message: "This password doesn't match" });
        }else{
            models.User.findOne({ email: email }).exec(async (err, user) => {
                if (user === null) {
                    let userObj = new models.User(req.body);
                    userObj.password = await bcrypt.hash(userObj.password, parseInt(process.env.SALTROUNDS));
                    userObj.save().then((user) => {
                        const token = jwt.sign({ id: user._id }, process.env.SECRET_JWT, {
                            expiresIn: 86400 // expires in 24 hours
                        });
                        res.status(200).send({ auth: true, token: token, message: "User created with success" });
    
                    });
                } else {
                    res.status(400).send({ auth: false, message: "Email already used" });
                }
            });
        }
    }
}