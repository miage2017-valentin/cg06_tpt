const models = require("../models");
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const mongoose = require('mongoose');


module.exports = class {

    async createClientDetection(req, res){
        const clientInput = req.body;
        const client = new models.ClientTfDetection(clientInput);
        client._id = mongoose.Types.ObjectId();
        client.password = await bcrypt.hash(client.password, parseInt(process.env.SALTROUNDS));
        try{
            await client.save();
            const clientReturn = await models.ClientTfDetection.findById(client._id, '-password')
            .exec();
            res.json(clientReturn);
        }catch(error){
            res.boom.badRequest("Fail to add a new client detection");
            return;
        }
    }

    async updateClientDetection(req, res){
        const clientInput = req.body;
        const id = req.params.id;

        const clientFound = await models.ClientTfDetection.findById(id).exec();
        if(clientFound === null){
            res.boom.notFound('Client not found');
            return;
        }

        if(clientInput.password !== undefined && clientInput.password !== ""){
            clientInput.password = await bcrypt.hash(clientInput.password, parseInt(process.env.SALTROUNDS));
        }else{
            clientInput.password = clientFound.password;
        }

        let client = Object.assign(clientFound, clientInput);

        try{
            await client.save();
            const clientReturn = await models.ClientTfDetection.findById(client._id, '-password')
            .exec();
            res.json(clientReturn);
        }catch(err){
            res.boom.badRequest("Fail to update this client detection");
            return;
        }
    }

    async getAllClientDetection(req, res){
        models.ClientTfDetection.find({},'-password').exec((error, clients) =>{
            res.json(clients);
        });
    }

    async deleteClient(req, res){
        const id = req.params.id;

        const clientFound = await models.ClientTfDetection.findById(id).exec();
        if(clientFound === null){
            res.boom.notFound('Client not found');
            return;
        }

        try{
            await models.ClientTfDetection.deleteOne({_id: id}).exec();
            res.json({message: "Client deleted with success"});
        }catch(error){
            res.boom.badRequest(error);
        }
    }
}