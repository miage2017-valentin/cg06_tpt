const jwt = require('jsonwebtoken');
const models = require("../models");
const Boom = require("boom");
const bcrypt = require('bcrypt');

module.exports = class {

    constructor(model) {
        this.models = model;
    }

    getMiddleware(req, res, next) {
        let token = req.headers.authorization;
        if (token !== undefined) {
            token = token.replace(/^Bearer /, "");
            try {
                const decoded = jwt.verify(token, process.env.SECRET_JWT, { ignoreExpiration: false });
                this.models.findById(decoded.id, "-password")
                    .exec((err, user) => {
                        if (user !== null) {
                            req.currentUser = user;
                            next();
                        } else {
                            res.boom.forbidden('User not exist');
                        }
                    })
            } catch (err) {
                console.log(err);
                res.boom.forbidden('No token provided.');
            }
        } else {
            res.boom.forbidden('No token provided.');
        }
    }

    login(req, res) {
        const login = req.body.login;
        const password = req.body.password;

        if(login === undefined){
            res.boom.unauthorized('Invalid user');
        }else{
            this.models.findOne({ login: login }).exec(async (err, user) => {
                if (user !== null) {
                    const valid = await bcrypt.compare(password, user.password);
                    if (valid) {
                        const token = jwt.sign({ id: user._id }, process.env.SECRET_JWT, {
                            expiresIn: 86400 // expires in 24 hours
                        });
                        res.status(200).send({ auth: true, token: token });
                    } else {
                        res.boom.unauthorized('Invalid password');
                    }
                } else {
                    res.boom.notFound('User not exist');
                }
            });
        }
    }

    checkToken(req, res){
        let token = req.query.token;
        if (token) {
            try {
                const decoded = jwt.verify(token, process.env.SECRET_JWT, { ignoreExpiration: false });
                this.model.findById(decoded.id, "-password")
                .exec((err, user) => {
                    if (user !== null) {
                        req.currentUser = user;
                        return res.status(200).send({ valid: true, user });
                    } else {
                        res.boom.badRequest('Invalid token');
                    }
                })
            } catch (err) {
                res.boom.badRequest('Token provided.');
            }
        } else {
            res.boom.badRequest('No token provided.');
        }
    }

    profile(req, res){
        return res.status(200).send({user: req.currentUser});
    }
}