import { Component, OnInit, Input } from '@angular/core';
import { Logs } from '@app/_models/logs';
import { FileService } from '@app/_services/file.service';

@Component({
  selector: 'app-logs-details',
  templateUrl: './logs-details.component.html',
  styleUrls: ['./logs-details.component.css']
})
export class LogsDetailsComponent implements OnInit {

  @Input('logs')
  private logs: Logs;

  constructor(private fileService: FileService) { }

  ngOnInit() {
    this.fileService.get(this.logs.image).toPromise().then(async (res : any)=>{
      this.logs.screenshot = await this.fileService.createImageFromBlob(res)
    })
  }

}
