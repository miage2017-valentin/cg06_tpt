import { Component, OnInit, Input } from '@angular/core';
import { User } from '@app/_models';
import { UserService } from '@app/_services';
import { MatSnackBar } from '@angular/material';

@Component({
  selector: 'app-edit-client',
  templateUrl: './edit-client.component.html',
  styleUrls: ['./edit-client.component.css']
})
export class EditClientComponent implements OnInit {

  @Input('client')
  private client: User;
  private editClient: User;

  constructor(private userService: UserService,
    private snackBar: MatSnackBar) { }

  ngOnInit() {
    this.editClient = {...this.client};
  }

  edit(){
    this.userService.update(this.editClient).subscribe(resp => {
      this.snackBar.open("Client updated successfully", null, {
        duration: 3000,
    });
    }, error =>{
      this.snackBar.open(error, null, {
          duration: 3000,
      });
    });
  }

}
