﻿import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { User } from '@app/_models';

@Injectable({ providedIn: 'root' })
export class UserService {
    constructor(private http: HttpClient) { }

    getAll() {
        return this.http.get<User[]>(`http://localhost:3001/clients/`);
    }

    getById(id: string) {
        return this.http.get(`http://localhost:3001/client/${id}`);
    }

    register(user: User) {
        return this.http.post(`http://localhost:3001/client`, user);
    }

    update(user: User) {
        return this.http.put(`http://localhost:3001/client/${user._id}`, user);
    }

    delete(id: string) {
        return this.http.delete(`http://localhost:3001/client/${id}`);
    }
}