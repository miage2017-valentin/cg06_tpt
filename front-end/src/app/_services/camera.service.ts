import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Camera } from '../_models/camera'
@Injectable({
  providedIn: 'root'
})
export class CameraService {

  constructor(private http: HttpClient) {
  }


  getAll() {
    return this.http.get<Camera[]>(`http://localhost:3001/cameras`);
  }

  getById(id: number) {
    return this.http.get(`http://localhost:3001/incidents/${id}`);
  }

  register(cam: Camera) {
    return this.http.post(`http://localhost:3001/camera`, cam);
  }

  
  update(cam: Camera) {
    return this.http.put(`http://localhost:3001/camera/${cam._id}`, cam);
  }

  delete(id: number) {
    return this.http.delete(`http://localhost:3001/camera/${id}`);
  }
}
