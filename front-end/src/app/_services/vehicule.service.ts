import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Vehicule } from '@app/_models';

@Injectable({ providedIn: 'root' })
export class VehiculeService {
    constructor(private http: HttpClient) { }

    getAll() {
        return this.http.get<Vehicule[]>(`http://localhost:3001/vehicles/`);
    }

    getById(id: number) {
        return this.http.get(`http://localhost:3001/vehicles/${id}`);
    }

    register(vehicule: any) {
        return this.http.post<any>(`http://localhost:3001/vehicle`, vehicule);
    }

    update(vehicule: Vehicule) {
        return this.http.put(`http://localhost:3001/vehicles/${vehicule.id}`, vehicule);
    }

    delete(id: number) {
        return this.http.delete(`http://localhost:3001/vehicle/${id}`);
    }
}