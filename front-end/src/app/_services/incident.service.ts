import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Event } from '@app/_models';

@Injectable({ providedIn: 'root' })
export class IncidentService {
    constructor(private http: HttpClient) { }

    getAll() {
        return this.http.get<Event[]>(`http://localhost:3001/events`);
    }

    getById(id: number) {
        return this.http.get(`http://localhost:3001/incidents/${id}`);
    }

    register(incident: Event) {
        return this.http.post(`http://localhost:3001/incidents/register`, incident);
    }

    update(incident: Event) {
        return this.http.put(`http://localhost:3001/incidents/${incident._id}`, incident);
    }

    delete(id: number) {
        return this.http.delete(`http://localhost:3001/incidents/${id}`);
    }
}