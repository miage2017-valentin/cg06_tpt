import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Event } from '@app/_models';

@Injectable({ providedIn: 'root' })
export class FileService {
    constructor(private http: HttpClient) { }

    get(id: String) {
        return this.http.get('http://localhost:3001/file/'+id, { responseType: 'blob' });
    }

    createImageFromBlob(image: Blob) {
        return new Promise((resolve, reject)=>{
            let reader = new FileReader();
            reader.addEventListener("load", () => {
               resolve(reader.result);
            }, false);
         
            if (image) {
               reader.readAsDataURL(image);
            }
        });
    }
}