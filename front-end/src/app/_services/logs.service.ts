import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Event } from '@app/_models';
import { Logs } from '@app/_models/logs';

@Injectable({ providedIn: 'root' })
export class LogsService {
    constructor(private http: HttpClient) { }

    getAll() {
        return this.http.get<Logs[]>(`http://localhost:3001/logs`);
    }
}