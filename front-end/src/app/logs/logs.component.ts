import { Component, OnInit, ViewChild } from '@angular/core';
import { Logs } from '@app/_models/logs';
import { MatTableDataSource, MatPaginator } from '@angular/material';
import { LogsService } from '@app/_services/logs.service';

@Component({
  selector: 'app-logs',
  templateUrl: './logs.component.html',
  styleUrls: ['./logs.component.css']
})
export class LogsComponent implements OnInit {

  private selectedLogs: Logs;
  private logs: Logs[];
  dataSource = new MatTableDataSource(this.logs);
  displayedColumns: string[] = ['Type', 'Color', 'Camera', 'Created Date', 'Actions'];

  @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor(private logsService: LogsService) { }

  ngOnInit() {
    this.refreshList();
  }

  refreshList(){
    this.logsService.getAll().subscribe(logs => {
      this.logs = logs;
      this.dataSource.data = logs;
      this.dataSource.paginator = this.paginator;
    });
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  setSelectedElement(element){
      this.selectedLogs = element;
      if(element === undefined){
        this.refreshList();
      }
  }
}
