﻿import { Routes, RouterModule } from '@angular/router';

import { HomeComponent } from './home';
import { VehiculesComponent } from './vehicules';
import { IncidentsComponent } from './incidents';
import { ListUserComponent } from './list-user';
import { LoginComponent } from './login';
import { AuthGuard } from './_guards';
import { CameraComponent } from './camera/camera.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { LogsComponent } from './logs/logs.component';

const appRoutes: Routes = [
    { path: '', component: DashboardComponent, canActivate: [AuthGuard] },
    { path: 'dashboard', component: DashboardComponent, canActivate: [AuthGuard] },
    { path: 'home', component: HomeComponent, canActivate: [AuthGuard], outlet: "home" },
    { path: 'login', component: LoginComponent },
    { path: 'vehicle', component: VehiculesComponent,
    outlet: "home" },
    { path: 'event', component: IncidentsComponent,
    outlet: "home" },
    { path: 'client-detection', component: ListUserComponent,
    outlet: "home" },
    { path: 'camera', component: CameraComponent,
    outlet: "home" },
    { path: 'logs', component: LogsComponent,
    outlet: "home" },

    // otherwise redirect to home
    { path: '**', redirectTo: '' }
];

export const routing = RouterModule.forRoot(appRoutes);