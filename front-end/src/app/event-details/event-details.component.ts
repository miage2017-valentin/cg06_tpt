import { Component, OnInit, Input } from '@angular/core';
import { FileService } from '@app/_services/file.service';
import { Event } from '@app/_models';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-event-details',
  templateUrl: './event-details.component.html',
  styleUrls: ['./event-details.component.css']
})
export class EventDetailsComponent implements OnInit {

  @Input("event")
  private event: Event;
  
  constructor(private fileService: FileService, private sanitizer: DomSanitizer) { }

  ngOnInit() {
    this.fileService.get(this.event.image).toPromise().then(async (res : any)=>{
      this.event.screenshot = await this.fileService.createImageFromBlob(res)
    })
  }
}
