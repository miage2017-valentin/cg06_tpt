import { Component, OnInit, Input } from '@angular/core';
import { Camera } from '@app/_models/camera';
import { MatSnackBar } from '@angular/material';
import { CameraService } from '@app/_services/camera.service';
import { UserService } from '@app/_services';
import { User } from '@app/_models';

@Component({
  selector: 'app-edit-camera',
  templateUrl: './edit-camera.component.html',
  styleUrls: ['./edit-camera.component.css']
})
export class EditCameraComponent implements OnInit {

  @Input("camera")
  private camera: Camera;
  private sendedCamera: Camera;
  private clientList: User[];

  constructor(private cameraService: CameraService, private userService: UserService,
    private snackBar: MatSnackBar) { }

  ngOnInit() {
    this.sendedCamera = { ...this.camera };
    this.userService.getAll().subscribe(
      resp => {
        this.clientList = resp;
      }
    )
  }

  edit(){
    let sendedCameraClone = { ...this.sendedCamera };
    sendedCameraClone.clientAssigned = this.sendedCamera.clientAssigned._id;
    this.cameraService.update(sendedCameraClone).subscribe(resp => {
      this.snackBar.open("Camera updated successfully", null, {
        duration: 3000,
    });
    }, error =>{
      this.snackBar.open(error, null, {
          duration: 3000,
      });
    });
  }
}
