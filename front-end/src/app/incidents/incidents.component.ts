﻿import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { Subscription } from 'rxjs';
import { first } from 'rxjs/operators';

import { User, Event } from '@app/_models';
import { IncidentService, AuthenticationService } from '@app/_services';
import { MatTableDataSource, MatPaginator } from '@angular/material';
import * as moment from 'moment';

@Component({ templateUrl: 'incidents.component.html' })
export class IncidentsComponent implements OnInit, OnDestroy {
    currentUser: User;
    public searchText : string;
    currentUserSubscription: Subscription;
    events: Event[] = [];
    dataSource = new MatTableDataSource(this.events);
    displayedColumns: string[] = ['Details', 'Type', 'Camera', 'Date', 'Actions'];
    selectedEvent: Event;

    @ViewChild(MatPaginator) paginator: MatPaginator;

    constructor(
        private authenticationService: AuthenticationService,
        private incidentService: IncidentService
    ) {
        this.currentUserSubscription = this.authenticationService.currentUser.subscribe(user => {
            this.currentUser = user;
        });
    }

    ngOnInit() {
        this.loadAllIncidents();
    }

    ngOnDestroy() {
        // unsubscribe to ensure no memory leaks
        this.currentUserSubscription.unsubscribe();
    }

    deleteIncident(id: number) {
        this.incidentService.delete(id).pipe(first()).subscribe(() => {
            this.loadAllIncidents()
        });
    }

    private loadAllIncidents() {
        this.incidentService.getAll().pipe(first()).subscribe(events => {
            events.map(elem => elem.formatDate = moment(elem.date).format('DD/MM/YYYY HH:mm:ss'));
            this.dataSource.data = events;
            this.dataSource.paginator = this.paginator;
        });
    }

    applyFilter(filterValue: string) {
        this.dataSource.filter = filterValue.trim().toLowerCase();
    }

    setSelectedElement(element){
        this.selectedEvent = element;
    }
}