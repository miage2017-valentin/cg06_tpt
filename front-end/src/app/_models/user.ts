﻿export class User {
    _id: string;
    login: string;
    password: string;
    reference: string;
    token: string;
}