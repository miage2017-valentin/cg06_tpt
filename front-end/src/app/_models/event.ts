import {Camera} from './camera'
export interface Event {
    _id: String,
    details: String,
    type: String,
    image: String,
    camera: Camera,
    date: Date,
    formatDate: String,
    screenshot: any
}