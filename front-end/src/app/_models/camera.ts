export class Camera {
    _id: number;
    reference: string;
    url: string;
    clientAssigned: any;
}