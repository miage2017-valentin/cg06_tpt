import {Camera} from './camera'
export interface Logs {
    _id: String,
    type: String,
    image: String,
    camera: Camera,
    createdDate: Date,
    screenshot: any
}