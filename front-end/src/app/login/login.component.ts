﻿import { Component, OnInit, Output } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { first } from 'rxjs/operators';

import { AlertService, AuthenticationService } from '@app/_services';
import { MatSnackBar } from '@angular/material';
import { EventEmitterService } from '@app/event-emitter-service';

@Component({templateUrl: 'login.component.html'})
export class LoginComponent implements OnInit {
    loginForm: FormGroup;
    loading = false;
    submitted = false;
    returnUrl: string;
    username: string;
    password: string;
    emailFormControl = new FormControl('', [
        Validators.required,
        Validators.email,
      ]);

    passwordFormControl = new FormControl('', [
        Validators.required
      ]);

    constructor(
        private formBuilder: FormBuilder,
        private route: ActivatedRoute,
        private router: Router,
        private authenticationService: AuthenticationService,
        private alertService: AlertService,
        private snackBar: MatSnackBar,
        private eventEmitter: EventEmitterService
    ) {
        // redirect to home if already logged in
        if (this.authenticationService.currentUserValue) { 
            this.router.navigate(['/']);
        }
    }

    ngOnInit() {
        // get return url from route parameters or default to '/'
        this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
    }

    onSubmit() {
        
        if(this.passwordFormControl.hasError('required') || this.emailFormControl.hasError('required')){
            return false;
        }

        this.loading = true;
        this.authenticationService.login(this.username, this.password)
            .pipe(first())
            .subscribe(
                data => {
                    this.router.navigate([{outlets: {primary: 'dashboard', home: 'home'}}]);
                    this.eventEmitter.sendLoginSuccess(this.username, this.password);
                },
                error => {
                    this.snackBar.open(error, null, {
                        duration: 3000,
                    });
                });
    }
}
