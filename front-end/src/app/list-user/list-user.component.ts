import { Component, OnInit, Inject, ViewChild } from '@angular/core';
import { Router } from "@angular/router";
import { User } from "@app/_models";
import { UserService } from "@app/_services";
import { MatTableDataSource, MatPaginator, MatSnackBar } from '@angular/material';

@Component({
  selector: 'app-list-user',
  templateUrl: 'list-user.component.html',
  styleUrls: ['list-user.component.css']
})
export class ListUserComponent implements OnInit {
  @ViewChild(MatPaginator) paginator: MatPaginator;

  users: User[];
  private newClient : User = new User();
  private selectedClient: User;
  dataSource = new MatTableDataSource(this.users);
  displayedColumns: string[] = ['Login', 'Reference', 'Actions'];

  constructor(private router: Router, private userService: UserService,
    private snackBar: MatSnackBar) { }

  ngOnInit() {
    this.refreshList();
  }

  refreshList(){
    this.userService.getAll()
    .subscribe(data => {
      this.users = data;
      this.dataSource.data = data;
      this.dataSource.paginator = this.paginator;
    });
  }

  deleteUser(user: User): void {
    this.userService.delete(user._id)
      .subscribe(data => {
        this.refreshList();
      })
  };

  editUser(user: any): void {
    console.log(user);

    window.localStorage.removeItem("editUserId");
    window.localStorage.setItem("editUserId", user._id.toString());
    window.localStorage.removeItem("username");
    window.localStorage.setItem("username", user.login.toString());
    window.localStorage.removeItem("reference");
    window.localStorage.setItem("reference", user.reference.toString());


    this.router.navigate(['edit-user']);
  };

  addClient(): void {
    this.userService.register(this.newClient).subscribe(resp => {
      this.newClient = new User();
      this.refreshList();
    }, error =>{
        this.snackBar.open(error, null, {
            duration: 3000,
        });
    });
  };

  setSelectedElement(element){
      this.selectedClient = element;
      if(element === undefined){
        this.refreshList()
      }
  }
}
