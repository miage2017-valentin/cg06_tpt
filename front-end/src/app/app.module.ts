﻿import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { ReactiveFormsModule }    from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';

// used to create fake backend

import { AppComponent }  from './app.component';
import { routing }        from './app.routing';

import { AlertComponent } from './_components';
import { JwtInterceptor, ErrorInterceptor } from './_helpers';
import { HomeComponent } from './home';
import { LoginComponent } from './login';
import { VehiculesComponent } from './vehicules';
import { IncidentsComponent } from './incidents';
import { ListUserComponent } from './list-user';
import { GrdFilterPipe } from './grd-filter.pipe';
import { FormsModule } from '@angular/forms';;
import { CameraComponent } from './camera/camera.component';
import { MatButtonModule, MatInputModule, MatSnackBarModule, MatTableModule,MatPaginatorModule, MatIconModule, MatSelectModule } from '@angular/material';
import { MatChipsModule, MatGridListModule } from '@angular/material';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { DashboardComponent } from './dashboard/dashboard.component';
import { LogsComponent } from './logs/logs.component';
import { EventDetailsComponent } from './event-details/event-details.component';
import { EditCameraComponent } from './edit-camera/edit-camera.component';;
import { EditClientComponent } from './edit-client/edit-client.component';
import { LogsDetailsComponent } from './logs-details/logs-details.component'

@NgModule({
    imports: [
        BrowserModule,
        ReactiveFormsModule,
        HttpClientModule,
        routing,
        FormsModule,
        BrowserAnimationsModule,
        MatButtonModule,
        MatInputModule,
        MatSnackBarModule,
        MatTableModule,
        MatPaginatorModule,
        MatIconModule,
        MatChipsModule,
        MatGridListModule,
        MatSelectModule
    ],
    declarations: [
        AppComponent,
        AlertComponent,
        HomeComponent,
        LoginComponent,
        VehiculesComponent,
        IncidentsComponent,
        ListUserComponent,
        GrdFilterPipe,
        CameraComponent ,
        DashboardComponent ,
        LogsComponent ,
        EventDetailsComponent ,
        EditClientComponent,
        EditCameraComponent,
        LogsDetailsComponent
    ],
    providers: [
        { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true },
        { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true }
    ],
    bootstrap: [AppComponent]
})

export class AppModule { }