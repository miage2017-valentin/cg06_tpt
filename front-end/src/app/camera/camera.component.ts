import { Component, OnInit, ViewChild } from '@angular/core';
import { Camera } from '@app/_models/camera';
import { CameraService } from '../_services/camera.service'
import { UserService } from '../_services/user.service'
import { User } from '@app/_models';
import { MatPaginator, MatTableDataSource, MatSnackBar } from '@angular/material';

@Component({
  selector: 'app-camera',
  templateUrl: './camera.component.html',
  styleUrls: ['./camera.component.css']
})
export class CameraComponent implements OnInit {

  private cameras: Camera[];
  private addCam = false;
  private newRef: string;
  private newUrl: string;
  private newUser: string;
  private clientList: User[];
  private newCamera: Camera = new Camera();
  private selectedCamera: Camera;

  dataSource = new MatTableDataSource(this.cameras);
  displayedColumns: string[] = ['Reference', 'Url', 'Actions'];
  
  @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor(private cameraService: CameraService, private userService: UserService,
    private snackBar: MatSnackBar) { }

  ngOnInit() {

    this.userService.getAll().subscribe(
      resp => {
        this.clientList = resp;
      }
    )

    this.refreshList();

  }

  refreshList(){
    this.cameraService.getAll().subscribe(resp => {
      this.cameras = resp;
      this.dataSource.data = this.cameras;
      this.dataSource.paginator = this.paginator;
    });
  }

  getClient(id: string) {
    let user;
    this.clientList.forEach(element => {

      if (element["_id"] === id) {
        user = element["login"];
      }
    });
    return user;
  }

  addCamera() {
    this.cameraService.register(this.newCamera).subscribe(resp => {
      this.newCamera = new Camera();
      this.refreshList();
    }, error =>{
      this.snackBar.open(error, null, {
          duration: 3000,
      });
    });
  }

  deleteCamera(id) {
    this.cameraService.delete(id).subscribe(resp => {
      this.refreshList();
    }, error =>{
      this.snackBar.open(error, null, {
          duration: 3000,
      });
    });
  }

  triggerCam() {
    this.addCam = !this.addCam;
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  setSelectedElement(element){
      this.selectedCamera = element;
      if(element === undefined){
        this.refreshList()
      }
  }
}