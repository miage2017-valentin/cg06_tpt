﻿import { Component } from '@angular/core';
import { Router } from '@angular/router';

import { AuthenticationService } from './_services';
import { User } from './_models';
import io from 'socket.io-client';
import { EventEmitterService } from './event-emitter-service';

@Component({ selector: 'app', templateUrl: 'app.component.html' })
export class AppComponent {
    currentUser: User;
    private socket;
    private nbNotif = 0;

    constructor(
        private router: Router,
        private authenticationService: AuthenticationService,
        private _eventEmiter: EventEmitterService
    ) {
        this.authenticationService.currentUser.subscribe(x => this.currentUser = x);
        const currentUser = this.authenticationService.currentUserValue;
        this.socket = io('http://localhost');
        this.socket.on('notify', () => {
            this.nbNotif++;
        });
    }

    logout() {
        this.authenticationService.logout();
        this.router.navigate([{ outlets: { primary: 'login', home: null } }]);
    }

    showEvents(){
        this.nbNotif = 0;
        this.router.navigate([{ outlets: { primary: 'dashboard', home: 'event' } }]);
    }
}