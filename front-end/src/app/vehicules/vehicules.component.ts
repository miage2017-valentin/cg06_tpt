﻿import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { Subscription } from 'rxjs';
import { first } from 'rxjs/operators';

import { User, Vehicule } from '@app/_models';
import { VehiculeService, AuthenticationService } from '@app/_services';
import { MatTableDataSource, MatPaginator, MatSnackBar } from '@angular/material';

@Component({ templateUrl: 'vehicules.component.html' })
export class VehiculesComponent implements OnInit, OnDestroy {
    @ViewChild(MatPaginator) paginator: MatPaginator;
    
    currentUser: User;
    currentUserSubscription: Subscription;
    vehicules: Vehicule[] = [];
    dataSource = new MatTableDataSource(this.vehicules);
    displayedColumns: string[] = ['Number Car Plate', 'Created At', 'Actions'];
    addVehicule = false;
    newPlaque: string;

    constructor(
        private authenticationService: AuthenticationService,
        private vehiculeService: VehiculeService,
        private snackBar: MatSnackBar
    ) {
        this.currentUserSubscription = this.authenticationService.currentUser.subscribe(user => {
            this.currentUser = user;
        });
    }

    ngOnInit() {
        this.loadAllVehicules();
    }

    ngOnDestroy() {
        // unsubscribe to ensure no memory leaks
        this.currentUserSubscription.unsubscribe();
    }

    deleteVehicule(id: number) {
        this.vehiculeService.delete(id).pipe(first()).subscribe(() => {
            this.loadAllVehicules()
        });
    }

    private loadAllVehicules() {

        this.vehiculeService.getAll().pipe(first()).subscribe(vehicles => {
            this.vehicules = vehicles;
            this.dataSource.data = vehicles;
            this.dataSource.paginator = this.paginator;
        });
    }

    add() {
        let newVehic = {
            "vehicleNumber": this.newPlaque
        }
        this.vehiculeService.register(newVehic).subscribe(resp => {
            this.loadAllVehicules();
        }, error =>{
            this.snackBar.open(error, null, {
                duration: 3000,
            });
        });

    }

    toggleAddVehicule() {
        this.addVehicule = !this.addVehicule;
    }

    applyFilter(filterValue: string) {
        this.dataSource.filter = filterValue.trim().toLowerCase();
    }
}