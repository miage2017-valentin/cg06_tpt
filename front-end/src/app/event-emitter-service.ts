import { Injectable, EventEmitter } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class EventEmitterService {
  loginSuccess = new EventEmitter();

  constructor() { }

  sendLoginSuccess(login: string, password: String) {
    this.loginSuccess.emit({login, password});
  }
}
