# Pré-requis
 - Docker
 - Node 10

# Lancement de la base de donnée
```
docker-compose up
```

# Lancement de l'API : Développement
```
cd api
npm install
npm run-script dev
```

# Lancement de l'API : Production
```
cd api
npm install
npm start
```

# Lancement du front-end
```
cd front-end
npm install -g @angular/cli
npm install
ng serve
```
Accèder à l'URL suivante : http://localhost:4200